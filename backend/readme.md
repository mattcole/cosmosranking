# Cosmos Ranking

## Name
Matthew Cole

## Note/Disclaimer

This project copied some code from the class main project and adapted it to this project. In particular, some of the claims/authentication code is nearly identical. I figured there was no need to handwrite it again since a lot of it is boilerplate. I also copied some of the boilerplate for logging and layers.

## Build process

```
docker compose up
sqlx migrate run
cargo run
```

## What was built

A wep app that allows users to view NASA's APOD picture of the day and allow users to vote on their favorites. Users are able to save their favorites and view them at the bottom of the page. The top of the page has a bar that displays what the top voted pictures are. The site is implemented as a one page application using HTMX. Corrospondingly, the REST api returns html responses to be injected into the page. What each API endpoint returns and why is listed in the API section. "/admin" contains the admin page which allows you to ban/unban users. "/promote" elevates a user to admin privleges for testing purposes.

## How it worked

It went well. I have become much more confident in my ability to work with Rust for web applications. I really love Rust's trait and macro system and really enjoyed using them to reduce code complexity and boilerplate. HTMX was fun to work with even if it added some challenge.

## What didn't work

I wrote some macros that ended up being less helpful than I had hoped. I wrote a html! macro that created a lazy static tera instance based on the string provided. I decided to write it so that it used the first argument provided to initialize the tera context. The thought behind this was that I was generating html for my endpoints based on a data object so I want to make it easy to take in an object and convert it into html. The issue was that I almost always needed more than one object to generate the html. This ultimately forced me to use json! to combine data. This is bad and ideally I should refactor my macro to take a list of objects to add to the context.

## What lessons were learned

I probably should have focused more on implementing a minimum viable product that satisfied the project requirements before add unnessesary features so that I didn't have a mad dash to finish the mimumum requirements near the end.

## API

### /pic
Returns a box containing a picture and a like/dislike button

### /register
Used to register a new user. Redirect to the login page without reseting variables in order to auto login

### /login
Logs in the user with a jwt token

### /protected
Tests if the user is logged in for testing purposes

### /get_top
Returns a list of the top rated pictures. When clicked, the pictures call /pic and inject that into the picture box

### /get_favorites
Returns the users favorited pictures. When clicked, the pictures call /pic and inject that into the picture box

### /add_favorites/:date
Called by the like button returned by /pic. Returns a list of pictures that will be injected into the favorites bar in order to update it. Also will update the top pictures bar on load in order to update it

### /remove_favorites/:date
Same as /add_favorites but removes it from favorites

### /ban/:id
Bans the user with the specified id. Must be logged in as an admin. Returns the list of users and roles so that the page can be updated

### /unban/:id
Same as /ban but unbans the user