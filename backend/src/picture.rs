use crate::*;
use axum::extract::State;
use axum::response::Html;
use axum::Form;
use chrono::Days;
use chrono::Utc;
use once_cell::sync::Lazy;
use reqwest::Client;
use serde::*;
use serde_json::json;
use sqlx::FromRow;
use sqlx::PgPool;

#[derive(Deserialize, Serialize, FromRow, Default)]
pub struct Picture {
    #[sqlx(flatten)] //https://docs.rs/sqlx/latest/sqlx/trait.FromRow.html
    #[serde(default)] //https://serde.rs/field-attrs.html
    #[serde(flatten)]
    id: utils::Id<Picture>,
    copyright: Option<String>,
    date: String,
    explanation: String,
    hdurl: String,
    media_type: String,
    service_version: String,
    title: String,
    url: String,
}
#[derive(Deserialize, Serialize, FromRow, Default)]
pub struct FavoritePicture {
    #[sqlx(flatten)]
    #[serde(flatten)]
    pic: Picture,
    is_fav: bool,
}

async fn get_from_api(date: &str) -> Result<Picture, anyhow::Error> {
    let response = static_expr!(Client::new() => Client).get(format!("https://api.nasa.gov/planetary/apod?api_key=3hhMkzc2y1K0lFJ11u20FpFpN8WpkjbC3W5uY2I8&date={}", date))
        .send()
        .await?;

    dbg!("Sent a request to the NASA API");
    let body = response.text().await?;
    let picture = serde_json::from_str(&body)?;
    Ok(picture)
}

async fn add_picture_to_datebase(
    pic: Picture,
    pool: &PgPool,
    claim: &Claims,
) -> Option<FavoritePicture> {
    let Picture {
        id: _,
        copyright,
        date,
        explanation,
        hdurl,
        media_type,
        service_version,
        title,
        url,
    } = pic;
    query_into!(
        pool,
        r#"
            INSERT INTO "pictures" (copyright, date, explanation, hdurl, media_type, service_version, title, url )
            VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
            RETURNING *, (EXISTS (Select * from favorites where favorites.picture_id = pictures.id and favorites.user_id = $9)) as is_fav
        "#,
        copyright,
        date,
        explanation,
        hdurl,
        media_type,
        service_version,
        title,
        url,
        claim.id
    )
}

pub async fn get_picture(
    am_database: &routes::Store,
    date: String,
    claim: &Claims,
) -> Result<FavoritePicture, AppError> {
    let pic: Option<FavoritePicture> = query_into!(
        &am_database.conn_pool,
        r#"
    SELECT *, (EXISTS (Select * from favorites where favorites.picture_id = pictures.id and favorites.user_id = $2)) as is_fav FROM pictures WHERE date = $1
    "#,
        date,
        claim.id
    );

    match pic {
        Some(val) => Ok(val),
        None => {
            let pic = get_from_api(&date).await?;
            add_picture_to_datebase(pic, &am_database.conn_pool, claim)
                .await
                .ok_or(DatabaseError)
        }
    }
}
#[derive(Serialize, Deserialize, sqlx::FromRow)]
pub struct PicRequest {
    date: String,
}

pub async fn get_picture_page(
    State(am_database): State<routes::Store>,
    claim: Claims,
    Form(PicRequest { mut date }): Form<PicRequest>,
) -> Result<Html<String>, AppError> {
    let picture = if date == "Today" {
        date = Utc::now().format("%Y-%m-%d").to_string();
        dbg!("Tried to access todays picture");
        dbg!(&date);
        if let Ok(pic) = get_picture(&am_database, date, &claim).await {
            Ok(pic)
        } else {
            date = Utc::now()
                .checked_sub_days(Days::new(1))
                .unwrap()
                .format("%Y-%m-%d")
                .to_string(); //If we fail then try yesterdays image
            get_picture(&am_database, date, &claim).await
        }
    } else {
        get_picture(&am_database, date, &claim).await
    };

    match picture {
        Err(_) => {
            html!(json!({}) => "<div style=\"text-align: center;\"><br>No picture for that date<br><br></div>")
        }
        Ok(pic) => html! {json!({"picture": pic, "claim" : claim}) => concat!(r##"
        <div style= "flex-grow=1"> 
            <div style="text-align: center;"><br>{{ picture.title }}<br><br></div>
            <div><img src={{ picture.url }} alt="{{ picture.title }}" class="center" height=600px width=auto> </div>
            <br>
            <div hx-target="#picture" hx-get="/pic?date={{ picture.date }}" hx-trigger="click delay:0.01s">
            {% if picture.is_fav %}
                <button type="button"
                id="fav_{{ picture.date }}"
                class="center"
                hx-get="/remove_favorites/{{ picture.id }}"
                hx-target="#favorites">❌</button>       
            {% else %}
                <button type="button"
                id="fav_{{ picture.date }}"
                class="center"
                hx-get="/add_favorites/{{ picture.id }}"
                hx-target="#favorites">🧡</button>
            {% endif %}
            </div>
            <br>
        </div>   
    "##)},
    }
}
