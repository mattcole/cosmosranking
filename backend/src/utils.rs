use serde::*;
use sqlx::FromRow;
use std::marker::PhantomData;

#[macro_export]
macro_rules! query_into {
    ($pool:expr, $query:expr, $( $item:expr ),*) => {
        sqlx::query_as(
            $query
        )
        $(
            .bind(&$item)
        )*
        .fetch_one($pool)
        .await
        .map_err(|u| {dbg!(&u); u})
        .ok()
    }
}
#[macro_export]
macro_rules! query_into_vec {
    ($pool:expr, $query:expr, $( $item:expr ),*) => {
        sqlx::query_as(
            $query
        )
        $(
            .bind(&$item)
        )*
        .fetch_all($pool)
        .await
        .map_err(|u| {dbg!(&u); u})
        .ok()
    }
}
#[derive(Deserialize, Serialize, FromRow, Default)]
pub struct Id<T> {
    id: Option<i32>,
    #[sqlx(skip)] //https://docs.rs/sqlx/latest/sqlx/trait.FromRow.html
    #[serde(default)] //https://serde.rs/field-attrs.html
    _phantom: PhantomData<T>, //https://codedamn.com/news/uncategorized/rusts-type-system-exploring-type-inference-phantom-data-associated-types
}
#[macro_export]
macro_rules! html {
    ($source:expr => $html:expr) => {{
        static TERA: once_cell::sync::Lazy<tera::Tera> = once_cell::sync::Lazy::new(|| {
            let mut internal = tera::Tera::default();
            internal.add_raw_template("main", $html).unwrap();
            internal
        });
        TERA.render("main", &tera::Context::from_serialize($source).unwrap())
            .map_err(|_| TeraError)
            .map(|u| u.into())
    }};
}
#[macro_export]
macro_rules! static_expr {
    ($contents:expr => $static_type:ty) => {{
        static VAL: once_cell::sync::Lazy<$static_type> = Lazy::new(|| $contents);
        &VAL
    }};
}
#[macro_export]
macro_rules! const_macro_str {
    ($name:ident = $value:expr) => {
        #[macro_export]
        macro_rules! $name {
            () => {
                $value
            };
        }
    };
}
