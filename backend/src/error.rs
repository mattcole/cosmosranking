use axum::response::{Html, IntoResponse, Response};
use axum::Json;
use http::StatusCode;
use serde_json::json;

#[derive(Debug)]
pub enum AppError {
    #[allow(dead_code)]
    Any(anyhow::Error),
    TeraError,
    InternalServerError,
    DatabaseError,
    InvalidToken,
    MissingCredentials,
    UserAlreadyExists,
    InvalidPassword,
    ErrorDuringFavoriteLookup,
    UnableToAddFavorite,
}

impl IntoResponse for AppError {
    fn into_response(self) -> Response {
        match self {
            AppError::Any(error) => {
                let error_message = error.to_string();
                let body = Json(json!({ "error": error_message }));
                (StatusCode::INTERNAL_SERVER_ERROR, body).into_response()
            }
            AppError::InvalidPassword => Html(
                "<script>alert(\"Invalid password!\");window.location.href = \"\\\\\";</script>",
            )
            .into_response(),
            _ => {
                let body = Json(json!({ "error": format!("{:?}", self) }));
                (StatusCode::INTERNAL_SERVER_ERROR, body).into_response()
            }
        }
    }
}

impl From<anyhow::Error> for AppError {
    fn from(value: anyhow::Error) -> Self {
        AppError::Any(value)
    }
}
