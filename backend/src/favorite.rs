use axum::{
    extract::{Path, State},
    response::Html,
};
use serde_derive::{Deserialize, Serialize};
use serde_json::json;
use sqlx::FromRow;

use crate::*;

#[derive(Deserialize, Serialize, FromRow, Default)]
pub struct Favorite {
    picture_id: i32,
    user_id: i32,
}

#[derive(Deserialize, Serialize, FromRow, Default)]
pub struct PictureWithFavorites {
    #[sqlx(flatten)]
    #[serde(flatten)]
    pic: Picture,
    fav_count: i64,
}

pub fn format_picture_vec(
    pictures: Vec<PictureWithFavorites>,
    reload_top: bool,
) -> Result<Html<String>, AppError> {
    html!(json!({"pictures" : pictures, "reload_top" : reload_top}) => r##"
        {% if reload_top %}
            <div hx-get="/get_top" hx-trigger="load" id="top" hx-target="#top"></div>
        {% endif %}
            <ul style="display: flex; flex-wrap: wrap; list-style-type: none; justify-content: center;">
        {% for picture in pictures -%}
            <li style="padding:10px"><img 
                src={{ picture.url }} alt="{{ picture.title }}" class="center" height=100px width=auto 
                    hx-get="/pic?date={{ picture.date }}"
                    hx-target="#picture"
                    hx-trigger="click">
                <p style="text-align: right; margin-top: 0px">{{ picture.fav_count }}🧡</p>
            </li>
        {%- endfor %}
        </ul>
    "##)
}

pub async fn get_top(State(am_database): State<Store>) -> Result<Html<String>, AppError> {
    let pictures = query_into_vec!(
        &am_database.conn_pool,
        r#"
            SELECT *, (SELECT count(*) from favorites WHERE pictures.id = favorites.picture_id) as fav_count from pictures ORDER BY fav_count DESC LIMIT 7
"#,
    ).ok_or(DatabaseError)?;
    format_picture_vec(pictures, false)
}

pub async fn get_favorite_list(
    am_database: Store,
    claim: Claims,
) -> Result<Html<String>, AppError> {
    let pictures = query_into_vec!(
        &am_database.conn_pool,
        r#"
            SELECT *, (SELECT count(*) from favorites WHERE pictures.id = favorites.picture_id) as fav_count from pictures, (SELECT picture_id FROM favorites WHERE user_id = $1) as favorites WHERE id = picture_id
"#,
        claim.id
    ).ok_or(ErrorDuringFavoriteLookup)?;
    format_picture_vec(pictures, true)
}

pub async fn get_favorite(
    State(am_database): State<Store>,
    OptionalClaims(claims): OptionalClaims,
) -> Result<Html<String>, AppError> {
    if let Some(claim) = claims {
        get_favorite_list(am_database, claim).await
    } else {
        Ok(Html("Not logged in!".to_string()))
    }
}

pub async fn remove_favorite(
    State(am_database): State<Store>,
    Path(pic_id): Path<i32>,
    OptionalClaims(claims): OptionalClaims,
) -> Result<Html<String>, AppError> //Result<Html<String>, AppError>
{
    if let Some(claim) = claims {
        let _: Option<Favorite> = query_into!(
            &am_database.conn_pool,
            r#"
                DELETE from favorites 
                WHERE picture_id=$1 and user_id=$2
                RETURNING *"#,
            pic_id,
            claim.id
        );
        //pictures.ok_or(UnableToAddFavorite)?;
        Ok(get_favorite_list(am_database, claim).await?)
    } else {
        Ok(Html("Not logged in!".to_string()))
    }
}

pub async fn add_favorite(
    State(am_database): State<Store>,
    Path(pic_id): Path<i32>,
    OptionalClaims(claims): OptionalClaims,
) -> Result<Html<String>, AppError> //Result<Html<String>, AppError>
{
    if let Some(claim) = claims {
        let _: Option<Favorite> = query_into!(
            &am_database.conn_pool,
            r#"
                INSERT INTO "favorites" (picture_id, user_id)
                VALUES ($1, $2)
                RETURNING *"#,
            pic_id,
            claim.id
        );
        //pictures.ok_or(UnableToAddFavorite)?;
        Ok(get_favorite_list(am_database, claim).await?)
    } else {
        Ok(Html("Not logged in!".to_string()))
    }
}
