use crate::*;
use axum::extract::State;
use axum::response::Html;
use chrono::{Days, Utc};
use serde_json::json;

#[derive(serde::Deserialize, serde::Serialize)]
struct RootPage {
    //claims : Option<Claims>,
    is_logged_in: bool,
}

pub async fn root(
    State(_am_database): State<Store>,
    OptionalClaims(claims): OptionalClaims,
) -> Result<Html<String>, AppError> {
    if let Some(claims) = claims {
        if claims.role == "Banned" {
            html!(json!({})=>"You are banned<style>align-text: center</style><title>You are banned</title>")
        } else {
            html! {json!({"claims": claims, "today" : Utc::now().checked_sub_days(Days::new(1)).unwrap().format("%Y-%m-%d").to_string()}) => r##"
            <!DOCTYPE html>
            <html lang="en">
            <style>
            .center {
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
            </style>

            <head>
                <meta charset="UTF-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <title>Cosmos Ranking</title>
            </head>
            <body>
            <div style="background-color:grey;margin: 0% 6%;padding: 0px 0px 50px 0px">
                <div style="text-align: center;"><br>Top Pictures<br><br></div>
                <div hx-get="/get_top" hx-trigger="load" id="top" ></div>
            </div>
            <!--show dashboard-->
            <form
                class="center"
                hx-get="/pic"
                hx-target="#picture">
                <label class="center" style="text-align:center" for="date">Enter a date to see the picture for that day!</label>
                <input class="center" type="date" id="date" name="date">
                <input class="center" type="submit">
            </form>
            <div hx-get="/pic?date={{ today }}" hx-trigger="load" id="picture" style="background-color:grey;margin: 0% 6%;padding: 0px 0px 50px 0px"></div>
            <div hx-get="/get_favorites" hx-trigger="load" id="favorites" style="background-color:grey;margin: 0% 6%;padding: 0px 0px 50px 0px"></div>
            </body>
            <script src="https://unpkg.com/htmx.org@1.9.4"></script>
            </html>  
        "##}
        }
    } else {
        html! {json!({}) => r##"
        <!DOCTYPE html>
        <html lang="en">

        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Cosmos Ranking</title>
        </head>
        <body>
        
        <!--show login form-->
        <h2>Login</h2>
        
        <form action="/login" method="post">
            <label for="email">Email:</label>
            <input type="text" id="email" name="email">
            <label for="password">Password:</label>
            <input type="password" id="password" name="password">
            <input type="submit" value="Login">
        </form>

        <h2>Register</h2>
        
        <form action="/register" method="post">
            <label for="email">Email:</label>
            <input type="text" id="email" name="email">
            <label for="password">Password:</label>
            <input type="password" id="password" name="password">
            <label for="confirm_password">Confirm Password:</label>
            <input type="password" id="confirm_password" name="confirm_password">
            <input type="submit" value="Register">
        </form>
        
        </body>
        </html>  
    "##}
    }
}
