use axum::{
    extract::{Path, State},
    response::{Html, Response},
};
use http::{
    header::{LOCATION, SET_COOKIE},
    HeaderValue, StatusCode,
};
use hyper::Body;
use jsonwebtoken::Header;
use serde_json::json;

use crate::*;

pub async fn admin_page(
    State(am_database): State<Store>,
    OptionalClaims(claims): OptionalClaims,
) -> Result<Html<String>, AppError> {
    if let Some(claim) = claims {
        if claim.role == "Admin" {
            return get_admin_page(&am_database).await.map(|u| {
                Html(u.0 + r#"<script src="https://unpkg.com/htmx.org@1.9.4"></script>"#)
            });
        }
    }
    html!(json!({}) => "This page requires administrator privleges")
}
pub async fn get_admin_page(am_database: &Store) -> Result<Html<String>, AppError> {
    html!(json!(
        {"Admin" : get_users_by_role(am_database, "Admin").await?,
        "User" : get_users_by_role(am_database, "User").await?,
        "Banned" : get_users_by_role(am_database, "Banned").await?}
    ) =>r##"
    <div id="page">
    <h1>Admins</h1>
    <ul>
    {% for i in Admin -%}
    <li>{{ i.email }}</li>
    {%- endfor %}
    </ul>
    <h1>User (click to ban)</h1>
    <ul>
    {% for i in User -%}
    <li hx-get="/ban/{{ i.email }}" hx-trigger="click" hx-target="#page">{{ i.email }}</li>
    {%- endfor %}
    </ul>
    <h1>Banned Users (click to unban)</h1>
    <ul>
    {% for i in Banned -%}
    <li hx-get="/unban/{{ i.email }}" hx-trigger="click" hx-target="#page">{{ i.email }}</li>
    {%- endfor %}
    </ul>
    </div>
    "##)
}

pub async fn ban_user(
    State(am_database): State<Store>,
    OptionalClaims(claims): OptionalClaims,
    Path(query): Path<String>,
) -> Result<Html<String>, AppError> {
    if let Some(claim) = claims {
        dbg!(&claim.role);
        if claim.role == "Admin" {
            set_user_role(&am_database, "Banned", &query).await?;
            return get_admin_page(&am_database).await;
        }
    }
    html!(json!({}) => "This action requires administrator privleges")
}
pub async fn unban_user(
    State(am_database): State<Store>,
    OptionalClaims(claims): OptionalClaims,
    Path(query): Path<String>,
) -> Result<Html<String>, AppError> {
    if let Some(claim) = claims {
        dbg!(&claim.role);
        if claim.role == "Admin" {
            set_user_role(&am_database, "User", &query).await?;
            return get_admin_page(&am_database).await;
        }
    }
    html!(json!({}) => "This action requires administrator privleges")
}
pub async fn get_users_by_role(am_database: &Store, role: &str) -> Result<Vec<User>, AppError> {
    query_into_vec!(
        &am_database.conn_pool,
        r#"
        SELECT email, password, id, role FROM users WHERE role = $1
        "#,
        role
    )
    .ok_or(DatabaseError)
}

pub async fn set_user_role(am_database: &Store, role: &str, email: &str) -> Result<(), AppError> {
    let _: User = query_into!(
        &am_database.conn_pool,
        r#"
        UPDATE users
        SET role=$2
        WHERE email=$1
        RETURNING *;
        "#,
        email,
        role
    )
    .ok_or(DatabaseError)?;
    Ok(())
}

pub async fn promote(
    State(am_database): State<Store>,
    OptionalClaims(claims): OptionalClaims,
) -> Result<Response<Body>, AppError> {
    if let Some(claim) = claims {
        let returned_user: User = query_into!(
            &am_database.conn_pool,
            r#"
        UPDATE users
        SET role='Admin'
        WHERE email=$1
        RETURNING *;
        "#,
            claim.email
        )
        .ok_or(DatabaseError)?;
        // at this point we've authenticated the user's identity
        // create JWT to return
        let claims = Claims {
            id: returned_user.id,
            email: returned_user.email,
            exp: 0,
            role: returned_user.role,
        };

        let token = jsonwebtoken::encode(&Header::default(), &claims, &KEYS.encoding)
            .map_err(|_| AppError::MissingCredentials)?;

        let cookie = cookie::Cookie::build("jwt", token).http_only(true).finish();

        let mut response = Response::builder()
            .status(StatusCode::FOUND)
            .body(Body::empty())
            .unwrap();

        response
            .headers_mut()
            .insert(LOCATION, HeaderValue::from_static("/"));
        response.headers_mut().insert(
            SET_COOKIE,
            HeaderValue::from_str(&cookie.to_string()).unwrap(),
        );
        Ok(response)
    } else {
        Err(MissingCredentials)
    }
}
