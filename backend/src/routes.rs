use crate::*;

use axum::response::Response;
use axum::routing::*;
use axum::Router;
use http::StatusCode;
use hyper::Body;
use sqlx::PgPool;

#[derive(Clone)]
pub struct Store {
    pub conn_pool: PgPool,
}

pub async fn app(pool: PgPool) -> Router {
    let db = Store { conn_pool: pool };

    let (cors_layer, trace_layer) = layers::get_layers();

    Router::new()
        // The router matches these FROM TOP TO BOTTOM explicitly!
        .route("/", get(root))
        .route("/pic", get(picture::get_picture_page))
        .route("/register", post(user::register))
        .route("/login", post(user::login))
        .route("/protected", get(user::protected))
        .route("/get_top", get(favorite::get_top))
        .route("/get_favorites", get(favorite::get_favorite))
        .route("/add_favorites/:id", get(favorite::add_favorite))
        .route("/remove_favorites/:id", get(favorite::remove_favorite))
        .route("/admin", get(admin::admin_page))
        .route("/promote", get(admin::promote))
        .route("/ban/:id", get(admin::ban_user))
        .route("/unban/:id", get(admin::unban_user))
        .route("/*_", get(handle_404))
        .layer(cors_layer)
        .layer(trace_layer)
        .with_state(db)
}

async fn handle_404() -> Response<Body> {
    Response::builder()
        .status(StatusCode::NOT_FOUND)
        .body(Body::from("The requested page could not be found"))
        .expect("error in 404")
}
