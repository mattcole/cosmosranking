use crate::*;
use argon2::Config;
use axum::extract::State;
use axum::response::{Redirect, Response};
use axum::{Form, Json};
use http::header::{LOCATION, SET_COOKIE};
use http::{HeaderValue, StatusCode};
use hyper::Body;
use jsonwebtoken::Header;
use serde_derive::{Deserialize, Serialize};
use serde_json::Value;

#[derive(Serialize, Deserialize, sqlx::FromRow)]
pub struct User {
    #[serde(default)]
    pub id: i32,
    pub email: String,
    pub password: String,
    #[serde(default = "default_user")]
    pub role: String,
}
fn default_user() -> String {
    "User".to_string()
}

#[derive(Serialize, Deserialize, sqlx::FromRow)]
pub struct UserSignup {
    pub email: String,
    pub password: String,
    pub confirm_password: String,
}

pub struct LoggedInUser {
    pub token: Claims,
}

pub async fn get_user(store: &Store, email: &str) -> Result<User, AppError> {
    sqlx::query_as::<_, User>(
        r#"
            SELECT email, password, id, role FROM users WHERE email = $1
        "#,
    )
    .bind(email)
    .fetch_one(&store.conn_pool)
    .await
    .map_err(|_| InvalidPassword)
}

pub async fn create_user(store: &Store, user: UserSignup) -> Result<Json<Value>, AppError> {
    // TODO: Encrypt/bcrypt user passwords
    let result = sqlx::query("INSERT INTO users(email, password) values ($1, $2)")
        .bind(&user.email)
        .bind(&user.password)
        .execute(&store.conn_pool)
        .await
        .map_err(|_| AppError::InternalServerError)?;

    if result.rows_affected() < 1 {
        Err(AppError::InternalServerError)
    } else {
        Ok(Json(
            serde_json::json!({"message": "User created successfully!"}),
        ))
    }
}
pub async fn register(
    State(database): State<Store>,
    Form(mut credentials): Form<UserSignup>,
) -> Result<Redirect, AppError> {
    // We should also check to validate other things at some point like email address being in right format

    if credentials.email.is_empty() || credentials.password.is_empty() {
        return Err(MissingCredentials);
    }

    if credentials.password != credentials.confirm_password {
        return Err(InvalidPassword);
    }

    // Check to see if there is already a user in the database with the given email address
    let existing_user = get_user(&database, &credentials.email).await;

    if existing_user.is_ok() {
        return Err(UserAlreadyExists);
    }

    // Here we're assured that our credentials are valid and the user doesn't already exist
    // hash their password
    let hash_config = Config::default();
    let salt = std::env::var("SALT").expect("Missing SALT");
    let hashed_password = match argon2::hash_encoded(
        credentials.password.as_bytes(),
        // If you'd like unique salts per-user, simply pass &[] and argon will generate them for you
        salt.as_bytes(),
        &hash_config,
    ) {
        Ok(result) => result,
        Err(_) => {
            return Err(AppError::Any(anyhow::anyhow!("Password hashing failed")));
        }
    };

    credentials.password = hashed_password;

    let _ = create_user(&database, credentials).await?;
    Ok(axum::response::Redirect::temporary("/login"))
}

pub async fn login(
    State(database): State<Store>,
    Form(creds): Form<User>,
) -> Result<Response<Body>, AppError> {
    if creds.email.is_empty() || creds.password.is_empty() {
        return Err(AppError::MissingCredentials);
    }

    let existing_user = get_user(&database, &creds.email).await?;

    let is_password_correct =
        match argon2::verify_encoded(&existing_user.password, creds.password.as_bytes()) {
            Ok(result) => result,
            Err(_) => {
                return Err(AppError::InternalServerError);
            }
        };

    if !is_password_correct {
        return Err(InvalidPassword);
    }

    // at this point we've authenticated the user's identity
    // create JWT to return
    let claims = Claims {
        id: existing_user.id,
        email: creds.email.to_owned(),
        exp: get_timestamp_after_8_hours(),
        role: existing_user.role,
    };

    let token = jsonwebtoken::encode(&Header::default(), &claims, &KEYS.encoding)
        .map_err(|_| AppError::MissingCredentials)?;

    let cookie = cookie::Cookie::build("jwt", token).http_only(true).finish();

    let mut response = Response::builder()
        .status(StatusCode::FOUND)
        .body(Body::empty())
        .unwrap();

    response
        .headers_mut()
        .insert(LOCATION, HeaderValue::from_static("/"));
    response.headers_mut().insert(
        SET_COOKIE,
        HeaderValue::from_str(&cookie.to_string()).unwrap(),
    );
    Ok(response)
}

pub async fn protected(claims: OptionalClaims) -> Result<String, AppError> {
    let claim = claims.0.ok_or(MissingCredentials)?;
    Ok(format!(
        "Welcome to the PROTECTED area :) \n Your claim data is: {}",
        claim
    ))
}
