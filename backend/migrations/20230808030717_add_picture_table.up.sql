-- Add up migration script here
CREATE TABLE pictures
(
    id                     serial       PRIMARY KEY,
    ---created_on             TIMESTAMPTZ  NOT NULL DEFAULT NOW(),
    copyright              TEXT,
    date                   TEXT         NOT NULL,
    explanation            TEXT         NOT NULL,
    hdurl                  TEXT         NOT NULL,
    media_type             TEXT         NOT NULL,
    service_version        TEXT         NOT NULL,
    title                  TEXT         NOT NULL,
    url                    TEXT         NOT NULL
);