-- Add up migration script here
CREATE TABLE IF NOT EXISTS roles
(
    role_name VARCHAR(255) PRIMARY KEY
);

INSERT INTO roles (role_name)
VALUES ('Admin');
INSERT INTO roles (role_name)
VALUES ('User');
INSERT INTO roles (role_name)
VALUES ('Banned');

ALTER TABLE users
ADD role VARCHAR(255) REFERENCES roles DEFAULT 'User';