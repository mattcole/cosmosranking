-- Add up migration script here
CREATE TABLE IF NOT EXISTS favorites
(
    picture_id integer REFERENCES pictures ON DELETE CASCADE,
    user_id integer REFERENCES users ON DELETE CASCADE,
    UNIQUE (picture_id, user_id)
)
