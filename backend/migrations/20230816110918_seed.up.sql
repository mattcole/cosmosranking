-- Add up migration script here
INSERT INTO pictures (copyright, date, explanation, hdurl, media_type, service_version, title, url)
VALUES ('', '2023-08-15', '', '', '', '', 'A Triply Glowing Night Sky over Iceland', 'https://apod.nasa.gov/apod/image/2308/TripleIceland_Zarzycka_6501.jpg');

INSERT INTO pictures (copyright, date, explanation, hdurl, media_type, service_version, title, url)
VALUES ('', '2023-08-08', '', '', '', '', 'Moon Meets Jupiter', 'https://apod.nasa.gov/apod/image/2308/MoonsJupiter_Coy_2630.jpg');

INSERT INTO pictures (copyright, date, explanation, hdurl, media_type, service_version, title, url)
VALUES ('', '2023-08-10', '', '', '', '', 'Five Meters over Mars', 'https://apod.nasa.gov/apod/image/2308/PIA25969_Ingenuity1024.jpg');